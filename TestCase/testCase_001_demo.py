# !/usr/bin/env python
# -*- coding:utf-8 -*-
# project name: Android_Ui_Automation
# author: "Lei Yong" 
# creation time: 2018/1/10 下午7:40
# Email: leiyong711@163.com
import unittest
from time import sleep

from wheel.signatures import assertTrue
from Business.authCode import msm
from Handle.positioning import Element
el = Element()

class Demo(unittest.TestCase):
    """登录"""
    driver = el.driver

    def guidePage(self):
        i=0
        sleep(5)
        el.swipeLeft(200)
        el.swipeLeft(200)
        el.tap([(525, 1702)], 200)
        if(el.findItem('com.android.packageinstaller:id/permission_allow_button')):
            el.localizeId('com.android.packageinstaller:id/permission_allow_button').click()
           #el.localizeuiAutomator('text("允许")').click()
        self.assertIsNotNone(el.localizeuiAutomator('text("首页")'),'没有进入首页')

    def testCase_001(self):
        """验证密码长度是否最多只可输入11位"""
        self.guidePage()
        el.localizeuiAutomator('text("我的")').click()
        el.localizeuiAutomator('text("登录/注册")').click()
        #手机号码长度超过11位
        el.localizeuiAutomator('text("请输入手机号码")').send_keys("152553915532222")
      #  print(el.localizeXpath("//android.widget.EditText[contains(@text,'15255')]").text)
        self.assertEqual('15255391553',el.localizeXpath("//android.widget.EditText[contains(@text,'15255')]").text)

    def testCase_002(self):
        """验证提示信息"""
        # 密码长度不够
        el.localizeuiAutomator('text("请输入密码")').send_keys("df123")
        el.localizeuiAutomator('text("登录")').click()
        self.assertTrue(el.isFindToast("密码最少为6位"))
        # 手机号码长度小于11位
        el.localizeXpath("//android.widget.EditText[contains(@text,'15255')]").clear()
        el.localizeuiAutomator('text("请输入手机号码")').send_keys("1525539155")
        el.localizeuiAutomator('text("登录")').click()
        self.assertTrue(el.isFindToast("手机号码必须为11位"))
        #密码错误
        el.localizeXpath("//android.widget.EditText[contains(@text,'15255')]").clear()
        el.localizeuiAutomator('text("请输入手机号码")').send_keys("15255391553")
        el.localizeuiAutomator('text("•••••")').clear()
        el.localizeuiAutomator('text("请输入密码")').send_keys("df123123")
        el.localizeuiAutomator('text("登录")').click()
        self.assertTrue(el.isFindToast("用户名或密码错误"))

    def testCase_003(self):
        """清空密码、显示密码、隐藏密码"""
        el.tap([(983, 147)], 200)
        el.localizeuiAutomator('text("登录/注册")').click()
        el.localizeuiAutomator('text("请输入手机号码")').send_keys("15255391553")
        el.localizeuiAutomator('text("请输入密码")').send_keys("df123123")
        #点击清空密码按钮
        el.tap([(836, 752)], 200)
        self.assertTrue(el.isFindElementByuiAutomator('text("请输入密码")'))
        #输入密码
        el.localizeuiAutomator('text("请输入密码")').send_keys("df123456")
        self.assertTrue(el.isFindElementByuiAutomator('text("••••••••")'))
        #点击小眼睛
        el.tap([(946, 752)], 200)
        self.assertTrue(el.isFindElementByuiAutomator('text("df123456")'))


    def testCase_004(self):
        """能否成功登陆"""
        el.tap([(983, 147)], 200)
        el.localizeuiAutomator('text("登录/注册")').click()
        el.localizeuiAutomator('text("请输入手机号码")').send_keys("15255391553")
        el.localizeuiAutomator('text("请输入密码")').send_keys("df.123456")
        el.localizeuiAutomator('text("登录")').click()
        self.assertIsNotNone(el.localizeuiAutomator('text("首页")'), '没有进入首页')
        self.driver.close_app()
        self.driver.close_app()

    def exit(self):
        """退出登录"""
        el.LocalizeDesc('我的').click()
        el.LocalizeDesc('my-set').click()
        el.LocalizeDesc('退出登录').click()