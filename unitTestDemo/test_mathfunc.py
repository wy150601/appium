# -*- coding: utf-8 -*-

import unittest
from mathfunc import *

''' 
一个class继承了unittest.TestCase，便是一个测试用例，但如果其中有多个以 test 开头的方法，
那么每有一个这样的方法，在load的时候便会生成一个TestCase实例，如：一个class中有四个test_xxx方法，
最后在load到suite中时也有四个测试用例。
'''

'''

可以看到，每一个用例的详细执行情况以及用例名，
用例描述均被输出了出来
（在测试方法下加代码示例中的”“”Doc String”“”，在用例执行时，
会将该字符串作为此用例的描述，加合适的注释能够使输出的测试报告更加便于阅读）

'''


class TestMathFunc(unittest.TestCase):
    """Test mathfuc.py"""
    def test_add(self):
        """Test method add(a, b)"""
        self.assertEqual(3, add(1, 2))
        self.assertNotEqual(3, add(2, 2))

    def test_minus(self):
        """Test method minus(a, b)"""
        self.assertEqual(1, minus(3, 2))

    def test_multi(self):
        """Test method multi(a, b)"""
        self.assertEqual(6, multi(2, 3))
    @unittest.skip('Do not run this.')
    def test_divide(self):
        """Test method divide(a, b)"""
        #self.skipTest('Do not run this.')
        self.assertEqual(2, divide(6, 3))
        self.assertEqual(2.5, divide(5, 2))

#====================================每个方法执行前后======================================
    # 如果我的测试需要在每次执行之前准备环境，或者在每次执行完之后需要进行一些清理怎么办？比如执行前需要连接数据库，执行完成之后需要还原数据、断开连接。总不能每个测试方法中都添加准备环境、清理环境的代码吧。
    # 这就要涉及到我们之前说过的testfixture了

    # 我们添加了
    # setUp()
    # 和
    # tearDown()
    # 两个方法（其实是重写了TestCase的这两个方法），这两个方法在每个测试方法执行前以及执行后执行一次，setUp用来为测试准备环境，tearDown用来清理环境，已备之后的测试

    def setUp(self):
        print "do something before test.Prepare environment."

    def tearDown(self):
        print "do something after test.Clean up."
#=================================================================================


#=================================================
# 如果想要在所有case执行之前准备一次环境，并在所有case执行结束之后再清理环境，我们可以用 setUpClass() 与 tearDownClass():
#
#     @classmethod
#     def setUpClass(cls):
#         print "This setUpClass() method only called once."
#
#     @classmethod
#     def tearDownClass(cls):
#         print "This tearDownClass() method only called once too."

#=================================================

#===========================跳过某个用例===========================
# skip装饰器一共有三个
# unittest.skip(reason)→
# unittest.skipIf(condition, reason)→
# unittest.skipUnless(condition, reason)→
# skip无条件跳过，skipIf当condition为True时跳过，skipUnless当condition为False时跳过。
#---------------------------------------------------
#TestCase.skipTest()方法

 # def test_divide(self):
 #        """Test method divide(a, b)"""
 #        self.skipTest('Do not run this.')
 #        print "divide"
 #        self.assertEqual(2, divide(6, 3))
 #        self.assertEqual(2.5, divide(5, 2))

#==================================================================

if __name__ == '__main__':
    unittest.main()



'''


'''